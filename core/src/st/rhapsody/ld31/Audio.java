package st.rhapsody.ld31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by nicklaslof on 07/12/14.
 */
public class Audio {
    public final static Sound SHOOT = Gdx.audio.newSound(Gdx.files.internal("shoot.wav"));
    public final static Sound ENEMY_SHOOT = Gdx.audio.newSound(Gdx.files.internal("enemy_shoot.wav"));
    public final static Sound ENEMY_HIT = Gdx.audio.newSound(Gdx.files.internal("enemy_hit.wav"));
    public final static Sound PLAYER_HIT = Gdx.audio.newSound(Gdx.files.internal("enemy_hit.wav"));
    public final static Sound ENEMY_DEAD = Gdx.audio.newSound(Gdx.files.internal("player_hit.wav"));
    public final static Sound GAME_OVER = Gdx.audio.newSound(Gdx.files.internal("game_over.wav"));
    public final static Sound HEAL_PICKUP = Gdx.audio.newSound(Gdx.files.internal("heal_pickup.wav"));



}
