package st.rhapsody.ld31.level;

import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld31.Audio;
import st.rhapsody.ld31.sprite.*;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Level {

    private static Array<Explosion> explosions = new Array<Explosion>();
    private static Array<HealthSpawner> healthSpawners = new Array<HealthSpawner>();
    private static boolean gameOver;
    private static boolean started = false;
    private final int xMax;
    private final int yMax;
    private final Texture floorTexture;
    private final Texture enemyTexture;
    public static Texture bulletTexture;
    private static Player player;
    private final Texture texture;
    private final Texture wallTexture;
    private static Array<Entity> entities = new Array<Entity>();
    public static Texture heartTexture;
    Random random = new Random();
    public static RayHandler rayHandler;
    private int[][] level;

    private final Vector2 tmpVector = new Vector2();
    private final Vector2 tmpVector2 = new Vector2();

    private static int AIR = 0;
    private static int ROOMSPACE = 1;
    private static int WALL = 2;
    private static int ENTRANCE = 3;
    private boolean debugRender;
    private static int playerScore;

    public Level(RayHandler rayHandler) {
        Level.rayHandler = rayHandler;
        texture = new Texture("player-wip.png");
        wallTexture = new Texture("wall.png");
        floorTexture = new Texture("floor.png");
        enemyTexture = new Texture("enemy.png");
        bulletTexture = new Texture("bullet.png");
        heartTexture = new Texture("heart.png");
        xMax = 1024 / 24;
        yMax = 576 / 24;

        regenerate();

    }

    public static int getPlayerScore() {
        return playerScore;
    }

    public static void increasePlayerScore(){
        playerScore++;
    }

    public void regenerate() {

        for (Entity sprite : entities) {
            sprite.dispose();
        }

        Iterator<Entity> iterator = entities.iterator();

        while(iterator.hasNext()){
            Entity next = iterator.next();
            if (next.isDisposeSprite()){
                next.removeFromPhysics();
                iterator.remove();
            }
        }


        entities.clear();
        level = new int[xMax+1][yMax+1];

        generateRoom(0,0,xMax, yMax, true);
        generateLevelRooms(xMax, yMax);


        Player player = new Player(rayHandler,texture,50,50);
        this.player = player;
        playerScore = 0;
        entities.add(player);

        int i = getRandomNumberInRange(6,20);

        for (int j = 0; j < i; j++) {
            spawnEnemy();
        }

        gameOver = false;
        started = false;
    }

    private void spawnEnemy(){
        int x = 0;
        int y = 0;
        boolean canSpawn = false;
        while (!canSpawn){
            x = getRandomNumberInRange(2,xMax);
            y = getRandomNumberInRange(2, yMax);

            if (level[x][y] == AIR || level[x][y] == ROOMSPACE){
                canSpawn = true;
            }
        }

        Enemy enemy = new Enemy(enemyTexture, x*24, y*24, 3, 1, Color.WHITE);
        entities.add(enemy);
    }

    private void generateRoom(int startX, int startY, int xMax, int yMax, boolean mainRoom) {
        for (int x = startX; x < xMax; x++) {
            for (int y = startY; y < yMax; y++) {
                if (level[x][y] == AIR){
                    Floor floor = new Floor(floorTexture, x * 24, y * 24);
                    entities.add(floor);
                }
                if (x == startX || x == xMax -1 || y == startY || y == yMax -1) {
                    if (level[x][y] != AIR || level[x][y] != ROOMSPACE || level[x][y] != ENTRANCE) {
                        if (!mainRoom){
                            if (random.nextFloat() > 0.09){
                                Wall wall = new Wall(wallTexture, x * 24, y * 24, 0);
                                entities.add(wall);
                                level[x][y] = WALL;
                            }else{
                                level[x][y] = ENTRANCE;
                            }
                        }else{
                            Wall wall = new Wall(wallTexture, x * 24, y * 24, 0);
                            entities.add(wall);
                            level[x][y] = WALL;
                        }

                    }
                }else if (!mainRoom){
                    if (level[x][y] != AIR || level[x][y] != ROOMSPACE) {
                        level[x][y] = ROOMSPACE;
                    }
                }
            }
        }
    }
    private void generateLevelRooms(int xMax, int yMax) {
        for (int x = 0; x < xMax; x++) {
            for (int y = 0; y < yMax; y++) {
                if (random.nextFloat() <0.009f){
                    //Wall wall = new Wall(wallTexture, x * 24, y * 24, 0);
                    //entities.add(wall);
                    int size = getRandomNumberInRange(5,12);
                    generateRoom(x, y, Math.min(x + size, xMax), Math.min(y + size, yMax), false);
                }
            }
        }
    }

    private int getRandomNumberInRange(int min, int max){
        int randomNumber = 0;

        while (randomNumber < min || randomNumber > max){
            randomNumber = random.nextInt(max);
        }

        return randomNumber;
    }

    public static Player getPlayer() {
        return player;
    }

    public void tick(){

        if (gameOver || !started) return;

        Iterator<Entity> iterator = entities.iterator();


        int numberOfEnmies = 0;

        while(iterator.hasNext()){
            Entity next = iterator.next();
            next.tick();

            if (next instanceof Enemy && !next.isDisposeSprite()){
                numberOfEnmies++;
            }

            if (next.isDisposeSprite()){
                next.removeFromPhysics();
                iterator.remove();
            }
        }

        Iterator<Explosion> iterator2 = explosions.iterator();

        while(iterator2.hasNext()){
            Explosion next = iterator2.next();
            next.tick();
            if (next.isDispose()){
                next.remove();
                iterator2.remove();
            }
        }

        if (numberOfEnmies < 6){
            spawnEnemy();
        }

        Iterator<HealthSpawner> iterator3 = healthSpawners.iterator();

        while(iterator3.hasNext()){
            HealthSpawner next = iterator3.next();

            Health health = new Health(heartTexture, next.getX(), next.getY());
            entities.add(health);
            iterator3.remove();
        }
    }


    public void render(float delta, SpriteBatch spriteBatch){
        for (Entity sprite : entities) {
            sprite.draw(spriteBatch);
        }
    }

    public void movePlayerUp() {
        player.moveUp();
    }

    public void movePlayerDown() {
        player.moveDown();
    }

    public void movePlayerLeft() {
        player.moveLeft();
    }

    public void movePlayerRight() {
        player.moveRight();
    }


    public void toggleDebugRender() {
        debugRender = !debugRender;
    }

    public boolean isDebugRender() {
        return debugRender;
    }

    public void playerShoot() {
        /*Vector2 angularVelocity = player.getAngularVelocity().cpy().nor();
        Bullet bullet = new Bullet(bulletTexture, player.getX()+(int)angularVelocity.x*100, player.getY()+(int)angularVelocity.y*100,player.getBulletDisposedCallback());
        entities.add(bullet);
        bullet.applyForce(angularVelocity);*/
        if (player.getBullets() < 3) {
            Entity closestEnemy = getClosestEnemy(player);
            if (closestEnemy != null) {

                shoot(player.getBulletDisposedCallback(), new Vector2(player.getX(), player.getY()), new Vector2(closestEnemy.getX(), closestEnemy.getY()), true);
            }
        }
    }

    private Entity getClosestEnemy(Entity entity) {
        tmpVector2.set(entity.getX(), entity.getY());
        float closestFoundDist = 1000;
        Entity closestEntityFound = null;
        for (Entity e : entities) {
            if (e.equals(entity)) continue;
            if (!(e instanceof LivingEntity)) continue;
            tmpVector.set(e.getX(), e.getY());
            float dst = tmpVector.dst(tmpVector2);
            if (dst < closestFoundDist){
                closestFoundDist = dst;
                closestEntityFound = e;
            }
        }
        return closestEntityFound;
    }

    public static void shoot(BulletDisposedCallback bulletDisposedCallback, Vector2 enemyPos, Vector2 destPos, boolean player) {
        Vector2 dir = destPos.sub(enemyPos);
        Bullet bullet;
        if (player){
            bullet = new PlayerBullet(bulletTexture, (int)enemyPos.x, (int) enemyPos.y, bulletDisposedCallback);
            Audio.SHOOT.play(0.5f);
        }else {
            bullet = new EnemyBullet(bulletTexture, (int) enemyPos.x, (int) enemyPos.y, bulletDisposedCallback);
            Audio.ENEMY_SHOOT.play();
        }
        entities.add(bullet);
        bullet.applyForce(dir.nor());
    }

    public static void showExplosion(float x, float y) {
        Explosion explosionLight = new Explosion(rayHandler, 365, Color.RED, Entity.convertToBox2d(x), Entity.convertToBox2d(y));
        explosionLight.setSoft(true);
        explosionLight.setSoftnessLength(0.4f);
        explosionLight.setActive(true);
        explosionLight.setXray(false);
        explosions.add(explosionLight);
    }

    public static void gameOver() {
        Audio.GAME_OVER.play();
        gameOver = true;
    }

    public static boolean isGameOver() {
        return gameOver;
    }

    public static void spawnHealth(float x, float y) {
        healthSpawners.add(new HealthSpawner((int)x,(int)y));
    }

    public static boolean isStarted() {
        return started;
    }

    public static void setStarted(){
        started = true;
    }
}
