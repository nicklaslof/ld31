package st.rhapsody.ld31.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import st.rhapsody.ld31.sprite.Bullet;
import st.rhapsody.ld31.sprite.Enemy;
import st.rhapsody.ld31.sprite.Entity;
import st.rhapsody.ld31.sprite.Player;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {

        Entity entity1 = (Entity) contact.getFixtureA().getBody().getUserData();
        Entity entity2 = (Entity) contact.getFixtureB().getBody().getUserData();

        entity1.onCollision(entity2);
        entity2.onCollision(entity1);



    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
