package st.rhapsody.ld31.screen;

import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld31.control.Input;
import st.rhapsody.ld31.level.Level;
import st.rhapsody.ld31.sprite.Entity;

public class GameScreen implements Screen {
	private SpriteBatch batch;
    private SpriteBatch ui_batch;
    private Level level;
    private Input input;
    private OrthographicCamera camera;
    private Box2DDebugRenderer debugRenderer;
    private RayHandler rayHandler;
    private FitViewport fitViewport;
    private BitmapFont font;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        input.update();

        Entity.tickPhysics();
        level.tick();


        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        level.render(delta, batch);
        batch.end();

        rayHandler.setCombinedMatrix(camera.combined.cpy().scl(100f));
        rayHandler.updateAndRender();

        if (level.isDebugRender()) {
            Entity.debugRender(debugRenderer, camera.combined.cpy());
        }

        renderUI();




        Gdx.graphics.setTitle("Trapped! ("+Gdx.graphics.getFramesPerSecond()+" fps)");
    }

    private void renderUI(){
        ui_batch.begin();
        ui_batch.setProjectionMatrix(camera.combined);

        if (Level.isGameOver()){

            font.draw(ui_batch,"Game over! (Press R to restart)",(1024/2)-100,576/2);

        }else if (!Level.isStarted()) {
            font.draw(ui_batch,"TRAPPED!",(1024/2)-50, (576/2)+240);
            font.draw(ui_batch,"Game by Nicklas Lof for",(1024/2)-90, (576/2)+210);
            font.draw(ui_batch,"Ludum Dare 31 - 48h compo",(1024/2)-100, (576/2)+180);
            font.draw(ui_batch,"Theme: Entire Game on One Screen",(1024/2)-130, (576/2)+150);
            font.draw(ui_batch,"Controls: WASD + SPACE",(1024/2)-95, (576/2)+100);
            font.draw(ui_batch,"R to restart or if stuck",(1024/2)-75, (576/2)+70);
            ui_batch.draw(Level.bulletTexture,1024/3,576/5, 16*20,16*20);
            font.draw(ui_batch,"Press SPACE to start",(1024/2)-70, (576/2)-70);

        }else{

            int health = Level.getPlayer().getHealth();
            int x = -10;
            int y = 0;
            for (int i = 0; i < health; i++) {
                x += 20;
                ui_batch.draw(Level.heartTexture, x, y, 32, 32);
            }
        }

        font.draw(ui_batch,"Score: "+Level.getPlayerScore(),20,566);

        ui_batch.end();
    }

    @Override
    public void resize(int width, int height) {
        fitViewport.update(width, height, true);
    }

    @Override
    public void show() {
        Entity.init();


        font = new BitmapFont();

        debugRenderer = new Box2DDebugRenderer(true,false,false,false,false,false);
        batch = new SpriteBatch();
        ui_batch = new SpriteBatch();
        rayHandler = Entity.getRayHandler();
        rayHandler.setAmbientLight(0.0f);
        level = new Level(rayHandler);
        input = new Input(level);
        Gdx.input.setInputProcessor(input);
        camera = new OrthographicCamera(0,0);
        camera.update();

        fitViewport = new FitViewport(1024, 576, camera);

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        ui_batch.dispose();
    }
}
