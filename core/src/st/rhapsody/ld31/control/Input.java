package st.rhapsody.ld31.control;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.IntIntMap;
import st.rhapsody.ld31.level.Level;

import static com.badlogic.gdx.Input.Keys.*;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Input implements InputProcessor {
    private Level level;
    private final IntIntMap keysPressed = new IntIntMap();

    public Input(Level level) {

        this.level = level;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == com.badlogic.gdx.Input.Keys.R){
            level.regenerate();
        }

        /*if (keycode == com.badlogic.gdx.Input.Keys.V){
            level.toggleDebugRender();
        }*/

        if (keycode == SPACE && !Level.isStarted()){
            level.setStarted();
            return true;
        }

        if (!Level.isGameOver()) {
            if (keycode == SPACE) {
                level.playerShoot();
            }

            keysPressed.put(keycode, keycode);
        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        keysPressed.remove(keycode,0);
        return true;
    }

    public void update(){
        if (keysPressed.containsKey(W) || keysPressed.containsKey(UP)){
            level.movePlayerUp();
        }

        if (keysPressed.containsKey(S) || keysPressed.containsKey(DOWN)){
            level.movePlayerDown();
        }

        if (keysPressed.containsKey(A) || keysPressed.containsKey(LEFT)){
            level.movePlayerLeft();
        }

        if (keysPressed.containsKey(D) || keysPressed.containsKey(RIGHT)){
            level.movePlayerRight();
        }
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
