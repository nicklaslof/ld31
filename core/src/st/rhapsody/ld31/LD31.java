package st.rhapsody.ld31;

import com.badlogic.gdx.Game;
import st.rhapsody.ld31.screen.GameScreen;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class LD31 extends Game {
    @Override
    public void create() {
        GameScreen gameScreen = new GameScreen();
        setScreen(gameScreen);
    }
}
