package st.rhapsody.ld31.sprite;

/**
 * Created by nicklaslof on 06/12/14.
 */
public interface BulletDisposedCallback {
    public void call();
}