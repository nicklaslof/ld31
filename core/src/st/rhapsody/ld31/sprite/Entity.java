package st.rhapsody.ld31.sprite;

import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import st.rhapsody.ld31.physics.WorldContactListener;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Entity {
    private int moveSpeed;
    private final Sprite sprite;
    protected float x;
    protected float y;
    private Vector2 tmpVector = new Vector2();
    private Vector2 tmpVector2 = new Vector2();
    protected final short PLAYER = 0x0001;
    protected final short ENEMY = 0x0002;
    protected final short PLAYER_BULLET = 0x0004;
    protected final short ENEMY_BULLET = 0x0008;
    protected final short WALL = 0x0010;
    protected final short HEALTH = 0x0020;

    private final short MASK_PLAYER = ENEMY | ENEMY_BULLET | WALL | HEALTH;
    private final short MASK_ENEMY = PLAYER | PLAYER_BULLET | WALL;
    private final short MASK_PLAYERBULLET = ENEMY | WALL;
    private final short MASK_ENEMYBULLET = PLAYER | WALL;
    private final short MASK_WALL = -1;
    private final short MASK_HEALTH = PLAYER | WALL;

    private static World world;
    protected Body body;
    private boolean disposeSprite;

    public static void init(){
        world = new World(new Vector2(0,0), true);
        world.setContactListener(new WorldContactListener());
    }

    public static RayHandler getRayHandler() {
        return new RayHandler(world);
    }

    public static float convertToBox2d(float x){
        return x * 0.01f;
    }

    public static float convertToWorld(float x){
        return x * 100f;
    }

    public boolean isDisposeSprite() {
        return disposeSprite;
    }

    private void addToBox(boolean staticBody, float dampening){
        BodyDef bodyDef = new BodyDef();

        bodyDef.type = staticBody ? BodyDef.BodyType.StaticBody : BodyDef.BodyType.DynamicBody;
        bodyDef.angle = 0;
        bodyDef.linearDamping = dampening;
        bodyDef.position.set(convertToBox2d(x), convertToBox2d(y));

        this.body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        if (staticBody){
            polygonShape.setAsBox(convertToBox2d(12),convertToBox2d(12));  // to start with.. probably need custom for each entity
        }else{
            polygonShape.setAsBox(convertToBox2d(8),convertToBox2d(8));  // to start with.. probably need custom for each entity
        }


        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 0;
        fixtureDef.restitution = 0;
        fixtureDef.isSensor = false;
        fixtureDef.shape = polygonShape;

        if (this instanceof Player){
            fixtureDef.filter.categoryBits = PLAYER;
            fixtureDef.filter.maskBits = MASK_PLAYER;
        }
        if (this instanceof Enemy){
            fixtureDef.filter.categoryBits = ENEMY;
            fixtureDef.filter.maskBits = MASK_ENEMY;
        }
        if (this instanceof PlayerBullet){
            fixtureDef.filter.categoryBits = PLAYER_BULLET;
            fixtureDef.filter.maskBits = MASK_PLAYERBULLET;
        }
        if (this instanceof EnemyBullet){
            fixtureDef.filter.categoryBits = ENEMY_BULLET;
            fixtureDef.filter.maskBits = MASK_ENEMYBULLET;
        }
        if (this instanceof Wall){
            fixtureDef.filter.categoryBits = WALL;
            fixtureDef.filter.maskBits = MASK_WALL;
        }

        if (this instanceof Health){
            fixtureDef.filter.categoryBits = HEALTH;
            fixtureDef.filter.maskBits = MASK_HEALTH;
        }

        body.createFixture(fixtureDef);
        polygonShape.dispose();

        body.setAwake(true);

        body.setUserData(this);

    }

    public static void tickPhysics() {
        world.step(1 / 60f,8,3);
    }

    public static void debugRender(Box2DDebugRenderer debugRenderer, Matrix4 debugMatrix){
        debugRenderer.render(world, debugMatrix.scl(100f));
    }

    public Entity(Texture texture, int x, int y, int moveSpeed, float dampening, float scale, Color color, boolean addToPhysics, boolean staticBody) {
        this.moveSpeed = moveSpeed;
        this.sprite = new Sprite(texture);
        this.sprite.scale(scale);
        if (color != null) {
            this.sprite.setColor(color);
        }

        this.x = x;
        this.y = y;
        if (addToPhysics) {
            addToBox(staticBody, dampening);
        }
    }

    public void tick(){
        if (body != null) {
            x = convertToWorld(body.getPosition().x) - 8;
            y = convertToWorld(body.getPosition().y) - 8;
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        sprite.setX(x);
        sprite.setY(y);
        sprite.draw(spriteBatch);
    }

    public void moveUp() {
        tmpVector2.set(0,1);
        applyImpulse(tmpVector2);

    }

    public void moveDown(){
        tmpVector2.set(0,-1);
        applyImpulse(tmpVector2);
    }

    public void moveRight(){
        tmpVector2.set(1,0);
        applyImpulse(tmpVector2);
    }

    public void moveLeft(){
        tmpVector2.set(-1,0);
        applyImpulse(tmpVector2);
    }

    private void applyImpulse(Vector2 direction){
        tmpVector.set(direction).scl(body.getMass() * moveSpeed * Gdx.graphics.getDeltaTime());
        body.applyLinearImpulse(tmpVector,Vector2.Zero, true);
    }

    public void dispose() {
        disposeSprite = true;
    }


    public int getX(){
        return (int) x;
    }

    public int getY(){
        return (int) y;
    }

    public void removeFromPhysics(){
        if (body != null) {
            world.destroyBody(body);
        }
    }

    public void applyForce(Vector2 angularVelocity) {
        applyImpulse(angularVelocity);
    }

    public void onCollision(Entity collidingEntity) {

    }
}
