package st.rhapsody.ld31.sprite;

/**
 * Created by nicklaslof on 07/12/14.
 */
public class HealthSpawner {
    private final int x;
    private final int y;

    public HealthSpawner(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
