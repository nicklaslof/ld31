package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld31.level.Level;

import java.util.Random;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Enemy extends LivingEntity {
    private final Random random;
    private int movingDirection;
    private final static int UP = 0;
    private final static int DOWN = 1;
    private final static int LEFT = 2;
    private final static int RIGHT = 3;
    private final Vector2 tmpVector = new Vector2();
    private final Vector2 tmpVector2 = new Vector2();
    private final BulletDisposedCallback bulletDisposedCallback;
    private int bullets = 0;
    private float timeElpasedSinceLastAttack;

    public Enemy(Texture texture, int x, int y, int moveSpeed, float scale, Color color) {
        super(texture, x, y, moveSpeed, 10f, scale, color, true,false, 2);
        random = new Random();
        bulletDisposedCallback = new BulletDisposedCallback(){

            @Override
            public void call() {
                if (bullets > 0){
                    bullets--;
                }
            }
        };
    }

    @Override
    public void tick() {

        super.tick();
        timeElpasedSinceLastAttack += Gdx.graphics.getDeltaTime();
        if (timeElpasedSinceLastAttack > 0.2) {
            if (random.nextFloat() < 0.05) {
                Player player = Level.getPlayer();
                timeElpasedSinceLastAttack = 0;
                if (player != null) {
                    tmpVector.set(player.getX(), player.getY());
                    tmpVector2.set(x, y);
                    float dst = tmpVector.dst(tmpVector2);
                    if (dst < 4 * 32 && bullets < 1) {
                        bullets++;
                        Level.shoot(bulletDisposedCallback, tmpVector2.cpy(), tmpVector.cpy(), false);
                    }
                }
            }
        }

        if (dead){
            dispose();
            return;
        }

        if (random.nextFloat() < 0.04f){
            movingDirection = random.nextInt(4);
        }

        switch (movingDirection){
            case UP: moveUp();break;
            case DOWN: moveDown();break;
            case LEFT: moveLeft();break;
            case RIGHT: moveRight();break;
        }
    }
}
