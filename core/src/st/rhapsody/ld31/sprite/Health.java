package st.rhapsody.ld31.sprite;

import box2dLight.PointLight;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import st.rhapsody.ld31.level.Level;

/**
 * Created by nicklaslof on 07/12/14.
 */
public class Health extends Entity {
    private final PointLight pointLight;
    private float lifeTime;

    public Health(Texture texture, int x, int y) {
        super(texture, x, y, 0, 0, 1, Color.WHITE, true, true);
        pointLight = new PointLight(Level.rayHandler, 365, Color.RED, 0.3f, convertToBox2d(x), convertToBox2d(y));
        pointLight.setSoft(true);
        pointLight.setSoftnessLength(0.3f);
        pointLight.setActive(true);
    }

    @Override
    public void tick() {
        super.tick();
        lifeTime += Gdx.graphics.getDeltaTime();
        if (lifeTime > 30){
            dispose();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        pointLight.remove();
    }
}
