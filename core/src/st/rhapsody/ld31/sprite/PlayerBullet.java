package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class PlayerBullet extends Bullet {
    public PlayerBullet(Texture texture, int x, int y, BulletDisposedCallback callback) {
        super(texture, x, y, Color.WHITE, callback);
    }
}
