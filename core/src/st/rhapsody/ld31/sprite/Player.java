package st.rhapsody.ld31.sprite;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Player extends LivingEntity{
    private final PointLight pointLight;
    private final BulletDisposedCallback bulletDisposedCallback;
    private int bullets = 0;
    private final short MASK_LIGHT = -1;

    public Player(RayHandler rayHandler, Texture texture, int x, int y) {
        super(texture, x, y, 10, 14f, 1, null, true, false,10);
        pointLight = new PointLight(rayHandler, 365, Color.ORANGE, 2f, convertToBox2d(x), convertToBox2d(y));
        pointLight.setSoft(true);
        pointLight.setSoftnessLength(0.4f);
        pointLight.setActive(true);

        Filter filter = new Filter();
        filter.maskBits = WALL;

        pointLight.setContactFilter(filter);

        bulletDisposedCallback = new BulletDisposedCallback(){

            @Override
            public void call() {
                if (bullets > 0){
                    bullets--;
                }
            }
        };
    }

    public int getBullets() {
        return bullets;
    }

    @Override
    public void tick() {
        super.tick();
        pointLight.setPosition(convertToBox2d(x+8),convertToBox2d(y+8));
        pointLight.update();
    }

    @Override
    public void dispose() {
        super.dispose();
        pointLight.remove();

    }

    @Override
    public void onCollision(Entity collisionEntity) {
        super.onCollision(collisionEntity);
    }

    public Vector2 getAngularVelocity() {
        return body.getLinearVelocity();
    }

    public BulletDisposedCallback getBulletDisposedCallback() {
        return bulletDisposedCallback;
    }
}
