package st.rhapsody.ld31.sprite;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by nicklaslof on 07/12/14.
 */
public class Explosion extends PointLight {

    private float lifeTime;
    private boolean dispose;

    public Explosion(RayHandler rayHandler, int rays, Color color, float x, float y) {
        super(rayHandler, rays, color, 2f, x, y);
    }

    public void tick(){
        lifeTime += Gdx.graphics.getDeltaTime();

        if (lifeTime > 0.1 &! dispose){
            setDistance(1.3f);
        }

        if (lifeTime > 0.15 &! dispose){
            setDistance(0.8f);
        }

        if (lifeTime > 0.18 &! dispose){
            setDistance(0.5f);
        }

        if (lifeTime > 0.2){
            dispose = true;
        }
    }

    public boolean isDispose() {
        return dispose;
    }
}
