package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Floor extends Entity {
    public Floor(Texture texture, int x, int y) {
        super(texture, x, y, 0, 0f, 1f, Color.GRAY, false,false);
    }
}
