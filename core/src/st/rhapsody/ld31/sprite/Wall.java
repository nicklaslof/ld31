package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Wall extends Entity {
    public Wall(Texture texture, int x, int y, int moveSpeed) {
        super(texture, x, y, moveSpeed, 0f, 1f, new Color(100,72,50,1), true, true);
    }
}
