package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class EnemyBullet extends Bullet{
    public EnemyBullet(Texture texture, int x, int y, BulletDisposedCallback callback) {
        super(texture, x, y, Color.RED, callback);
    }
}
