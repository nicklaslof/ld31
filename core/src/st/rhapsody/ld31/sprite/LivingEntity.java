package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import st.rhapsody.ld31.Audio;
import st.rhapsody.ld31.level.Level;

import javax.management.Query;
import java.util.Random;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class LivingEntity extends Entity {
    private int maxHealth;
    private int health;
    protected boolean dead = false;
    private Random random = new Random();

    public LivingEntity(Texture texture, int x, int y, int moveSpeed, float dampening, float scale, Color color, boolean addToPhysics, boolean staticBody, int maxHealth) {
        super(texture, x, y, moveSpeed, dampening, scale, color, addToPhysics, staticBody);
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }

    @Override
    public void onCollision(Entity collisionEntity) {
        super.onCollision(collisionEntity);
        if (collisionEntity instanceof Bullet){
            if (this instanceof Enemy && collisionEntity instanceof EnemyBullet){
                return;
            }

            if (this instanceof Player && collisionEntity instanceof PlayerBullet){
                return;
            }

            //((Bullet) collisionEntity).boom();
            hit(1);
        }

        if (collisionEntity instanceof Health && this instanceof Player){
            if (health < maxHealth) {
                health++;
                Audio.HEAL_PICKUP.play();
                collisionEntity.dispose();
            }


        }
    }

    @Override
    public void tick() {
        if (health < 1){
            dead = true;
        }


        super.tick();

    }

    protected void hit(int ammount) {

        health -= ammount;

        if (this instanceof Player){
            Audio.PLAYER_HIT.play();
            if (health < 1){

                Level.gameOver();
            }
        }
        if (this instanceof Enemy){
            if (health < 1){
                Audio.ENEMY_DEAD.play();
                Level.showExplosion(x,y);
                Level.increasePlayerScore();
                if (random.nextFloat() < 0.2){
                    Level.spawnHealth(x,y);
                }
            }else {
                Audio.ENEMY_HIT.play();
            }
        }
    }

    public int getHealth() {
        return health;
    }
}
