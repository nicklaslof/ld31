package st.rhapsody.ld31.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by nicklaslof on 06/12/14.
 */
public class Bullet extends Entity {
    private float age;
    private BulletDisposedCallback callback;

    public Bullet(Texture texture, int x, int y, Color color, BulletDisposedCallback callback) {
        super(texture, x, y, 60, 1f, 1, color, true, false);
        this.callback = callback;
    }

    @Override
    public void tick() {
        age += Gdx.graphics.getDeltaTime();
        super.tick();

        if (age > 0.9){
            dispose();
            callback.call();
        }

    }

    public void boom() {
        dispose();
        callback.call();
    }
}
