package st.rhapsody.ld31.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import st.rhapsody.ld31.LD31;
import st.rhapsody.ld31.screen.GameScreen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.vSyncEnabled = false;
        config.width = 1024;
        config.height = 576;
		new LwjglApplication(new LD31(), config);
	}
}
